Feature: Login
As a user, I want to be able to successfully log in to the local page so that I can access the resources and information that are restricted to logged-in users.
As a user, I also want to be able to log in to the SAP homepage after logging in to the local page, so that I can access the SAP resources and information that are restricted to logged-in users.
As a user, I want to be notified if my login attempt is unsuccessful so that I can correct my credentials and try again, or seek help if needed.

Scenario: Unvalid Login local page
Given I am on the login local page
When I type bad credentials
When I click the local login button
Then I should not be logged in local

Scenario: Login local page
Given I am on the login local page
When I enter valid local credentials
When I click the local login button
Then I should be logged in local

Scenario: Redirect to another website
Given I am on the login local page
When I enter valid local credentials
When I click the local login button
Then I am redirected to the SAP homepage


Scenario Outline: Check every credentials
  Given I am on the login local page
  When I enter Username as "<username>" and Password as "<password>"
  When I click the local login button
  Then I should be logged in local
  Examples:
    |username|password|
    |user1|pass1|
    |user2|pass2|
