package stepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LoginTest {
    private WebDriver driver;

    @Before
    public void setUp(){
        ChromeOptions chromeOptions = new ChromeOptions();
        this.driver = new ChromeDriver(chromeOptions);
    }

    @Given("I am on the login local page")
    public void iAmOnTheLoginPage() {
        driver.navigate().to("http://127.0.0.1:5500/login.html");
    }

    @When("I enter valid local credentials")
    public void iEnterValidCredentials() {
        // Find the username and password input fields
        WebElement usernameField = driver.findElement(By.id("username"));
        WebElement passwordField = driver.findElement(By.id("password"));

        // Enter the username and password
        usernameField.sendKeys("user1");
        passwordField.sendKeys("pass1");
    }

    @When("I type bad credentials")
    public void iEnterIncorrectCredentials() {
        // Find the username and password input fields
        WebElement usernameField = driver.findElement(By.id("username"));
        WebElement passwordField = driver.findElement(By.id("password"));

        // Enter the username and password
        usernameField.sendKeys("IncorrectTest");
        passwordField.sendKeys("IncorrectTest");
    }

    @When("I click the local login button")
    public synchronized void iClickTheLoginButton()  {
        // Find the login button and click it
        WebElement loginButton = driver.findElement(By.id("Submit"));
        loginButton.click();
    }

    @Then("I should be logged in local")
    public void iShouldBeLoggedIn() {
        // Check if the user is logged in by looking for the logout button
        WebElement logoutButton = driver.findElement(By.id("message"));
        String messageText = logoutButton.getText();
        assertTrue(messageText.contains("Login successful"));
    }

    @Then("I should not be logged in local")
    public void iShouldNotBeLoggedIn() {
        // Check if the user is logged in by looking for the logout button
        WebElement logoutButton = driver.findElement(By.id("message"));
        String messageText = logoutButton.getText();
        assertTrue(messageText.contains("Invalid username or password"));
    }

    @Then("I am redirected to the SAP homepage")
    public void i_am_redirected_to_the_SAP_homepage() {
        // Get the current URL of the page
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.urlToBe("https://www.sap.com/france/index.html?url_id=auto_hp_redirect_france"));
        String currentUrl = driver.getCurrentUrl();
        // Assert that the current URL is the SAP homepage
        assertEquals("https://www.sap.com/france/index.html?url_id=auto_hp_redirect_france", currentUrl);
    }
    @After
    public void closingBrowser() throws InterruptedException {
        sleep(2000);
        driver.quit();
    }


    @When("I enter Username as {string} and Password as {string}")
    public void iEnterUsernameAsAndPasswordAs(String arg0, String arg1) {
        WebElement usernameField = driver.findElement(By.id("username"));
        WebElement passwordField = driver.findElement(By.id("password"));

        // Enter the username and password
        usernameField.sendKeys(arg0);
        passwordField.sendKeys(arg1);
    }
}
